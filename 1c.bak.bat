@echo on
rem Можно раскоментить, если пользователи сами не выходят из 1с
rem "C:\Program Files (x86)\1cv8\8.3.9.2170\bin\ragent.exe" -stop
rem "C:\Program Files (x86)\1cv8\8.3.9.2170\bin\ragent.exe" -start
rem Вжух.
set date=%DATE%
set basename=profix
set logname=%basename%.%date%.log
rem Путь к базе.
set base="C:\bases_1c\TestBase"
rem Путь к бэкапу.
set bakdir=C:\testbak
rem Логин и пароль.
set user="backup"
set pass="123"
rem А это можно не трогать.
set logfile="%bakdir%\%basename%\%date%\%basename%.%date%.log
mkdir %bakdir%\%basename%\
mkdir %bakdir%\%basename%\%date%
rem Как запихать путь к 1с в переменную?
"C:\Program Files (x86)\1cv8\8.3.9.2170\bin\1cv8.exe" DESIGNER /F %base% /N %user% /P %pass% /DumpIB "%bakdir%\%basename%\%date%\%basename%.%date%.dt"
rem Пишем лог.
chcp 1251 >nul
setlocal
echo %date% > %logfile%
dir %bakdir%\%basename%\%date% >> %logfile%
echo "Backup OK" >> %logfile%
endlocal
rem
rem Ну, или так.
rem "C:\Program Files (x86)\1cv82\8.2.19.68\1cv8.exe" DESIGNER /S Srv01\InfoBase /N %user% /P %pass% /DumpIB "%bakdir%\%basename%\%date%\%basename%.%date%.dt"
